Rails.application.routes.draw do



  resources :mains 
  resources :shippers
  resources :carriers
  resources :endadds
  resources :startadds
  resources :bids
  resources :endaddresses
  resources :startaddresses
  resources :jobs do
  	member do
    	get :post
  	end
  	member do
    	get :bid
  	end  	
  end
  devise_for :users do
		  get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'    
		  put 'users' => 'devise/registrations#update', :as => 'user_registration'            
		end
  resources :users, :only => [:show]
  resources :posts
  resources :conversations, only: [:index, :show, :destroy] do
		  member do
		    post :reply
		  end
		  member do
		    post :restore
		  end	
		 collection do
	    	delete :empty_trash
	  	end  
	  	member do
	    	post :mark_as_read
	  	end
	end
  resources :messages, only: [:new, :create]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "mains#index"
end
