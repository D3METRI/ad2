require 'test_helper'

class ShipperControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get shipper_index_url
    assert_response :success
  end

  test "should get show" do
    get shipper_show_url
    assert_response :success
  end

end
