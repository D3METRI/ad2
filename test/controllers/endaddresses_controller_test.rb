require 'test_helper'

class EndaddressesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @endaddress = endaddresses(:one)
  end

  test "should get index" do
    get endaddresses_url
    assert_response :success
  end

  test "should get new" do
    get new_endaddress_url
    assert_response :success
  end

  test "should create endaddress" do
    assert_difference('Endaddress.count') do
      post endaddresses_url, params: { endaddress: { address: @endaddress.address, job_id: @endaddress.job_id, lattitude: @endaddress.lattitude, longitude: @endaddress.longitude } }
    end

    assert_redirected_to endaddress_url(Endaddress.last)
  end

  test "should show endaddress" do
    get endaddress_url(@endaddress)
    assert_response :success
  end

  test "should get edit" do
    get edit_endaddress_url(@endaddress)
    assert_response :success
  end

  test "should update endaddress" do
    patch endaddress_url(@endaddress), params: { endaddress: { address: @endaddress.address, job_id: @endaddress.job_id, lattitude: @endaddress.lattitude, longitude: @endaddress.longitude } }
    assert_redirected_to endaddress_url(@endaddress)
  end

  test "should destroy endaddress" do
    assert_difference('Endaddress.count', -1) do
      delete endaddress_url(@endaddress)
    end

    assert_redirected_to endaddresses_url
  end
end
