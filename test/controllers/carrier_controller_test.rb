require 'test_helper'

class CarrierControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get carrier_index_url
    assert_response :success
  end

  test "should get show" do
    get carrier_show_url
    assert_response :success
  end

end
