require 'test_helper'

class StartaddressesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @startaddress = startaddresses(:one)
  end

  test "should get index" do
    get startaddresses_url
    assert_response :success
  end

  test "should get new" do
    get new_startaddress_url
    assert_response :success
  end

  test "should create startaddress" do
    assert_difference('Startaddress.count') do
      post startaddresses_url, params: { startaddress: { address: @startaddress.address, lattitude: @startaddress.lattitude, longitude: @startaddress.longitude } }
    end

    assert_redirected_to startaddress_url(Startaddress.last)
  end

  test "should show startaddress" do
    get startaddress_url(@startaddress)
    assert_response :success
  end

  test "should get edit" do
    get edit_startaddress_url(@startaddress)
    assert_response :success
  end

  test "should update startaddress" do
    patch startaddress_url(@startaddress), params: { startaddress: { address: @startaddress.address, lattitude: @startaddress.lattitude, longitude: @startaddress.longitude } }
    assert_redirected_to startaddress_url(@startaddress)
  end

  test "should destroy startaddress" do
    assert_difference('Startaddress.count', -1) do
      delete startaddress_url(@startaddress)
    end

    assert_redirected_to startaddresses_url
  end
end
