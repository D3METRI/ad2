require 'test_helper'

class EndaddsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @endadd = endadds(:one)
  end

  test "should get index" do
    get endadds_url
    assert_response :success
  end

  test "should get new" do
    get new_endadd_url
    assert_response :success
  end

  test "should create endadd" do
    assert_difference('Endadd.count') do
      post endadds_url, params: { endadd: { address: @endadd.address, job_id: @endadd.job_id, lat: @endadd.lat, lng: @endadd.lng } }
    end

    assert_redirected_to endadd_url(Endadd.last)
  end

  test "should show endadd" do
    get endadd_url(@endadd)
    assert_response :success
  end

  test "should get edit" do
    get edit_endadd_url(@endadd)
    assert_response :success
  end

  test "should update endadd" do
    patch endadd_url(@endadd), params: { endadd: { address: @endadd.address, job_id: @endadd.job_id, lat: @endadd.lat, lng: @endadd.lng } }
    assert_redirected_to endadd_url(@endadd)
  end

  test "should destroy endadd" do
    assert_difference('Endadd.count', -1) do
      delete endadd_url(@endadd)
    end

    assert_redirected_to endadds_url
  end
end
