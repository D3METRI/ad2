require 'test_helper'

class StartaddsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @startadd = startadds(:one)
  end

  test "should get index" do
    get startadds_url
    assert_response :success
  end

  test "should get new" do
    get new_startadd_url
    assert_response :success
  end

  test "should create startadd" do
    assert_difference('Startadd.count') do
      post startadds_url, params: { startadd: { address: @startadd.address, job_id: @startadd.job_id, lat: @startadd.lat, lng: @startadd.lng } }
    end

    assert_redirected_to startadd_url(Startadd.last)
  end

  test "should show startadd" do
    get startadd_url(@startadd)
    assert_response :success
  end

  test "should get edit" do
    get edit_startadd_url(@startadd)
    assert_response :success
  end

  test "should update startadd" do
    patch startadd_url(@startadd), params: { startadd: { address: @startadd.address, job_id: @startadd.job_id, lat: @startadd.lat, lng: @startadd.lng } }
    assert_redirected_to startadd_url(@startadd)
  end

  test "should destroy startadd" do
    assert_difference('Startadd.count', -1) do
      delete startadd_url(@startadd)
    end

    assert_redirected_to startadds_url
  end
end
