class Job < ApplicationRecord
validates :description, :price, :trucktype, :title, :startad, :pickuptime, :dropofftime, :endad, :size, presence: true
	enum status: [:active, :inactive, :complete]
	belongs_to :user
	accepts_nested_attributes_for :user
	has_many :posts
  accepts_nested_attributes_for :posts
	has_many :bids
  accepts_nested_attributes_for :bids

  has_one :startadd
  accepts_nested_attributes_for :startadd

  has_one :endadd
  accepts_nested_attributes_for :endadd

	before_save :geocode_endpoints
	geocoded_by :startad, :latitude  => :startlat, :longitude => :startlng
	geocoded_by :endad, :latitude  => :endlat, :longitude => :endlng

def geocoder_init(options)
  unless defined?(@geocoder_options)
    @geocoder_options = {}
    require "geocoder/stores/#{geocoder_file_name}"
    include Geocoder::Store.const_get(geocoder_module_name)
  end
  @geocoder_options.merge! options
end

	private
  def geocode_endpoints
    if startad_changed?
      geocoded = Geocoder.search(startad).first
      if geocoded
        self.startlat = geocoded.latitude
        self.startlng = geocoded.longitude
      end
    end
    if endad_changed?
      geocoded = Geocoder.search(endad).first
      if geocoded
        self.endlat = geocoded.latitude
        self.endlng = geocoded.longitude
      end
    end
  end


  def same_elements?(array1, array2)
    array1.to_set == array2.to_set
  end


end
