class Startadd < ApplicationRecord
  belongs_to :job
  geocoded_by :address, :latitude  => :lat, :longitude => :lng
  after_validation :geocode 
end
