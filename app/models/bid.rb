class Bid < ApplicationRecord
	enum bidstatus: [:submitted, :rejected, :accepted]	
	belongs_to :user
	belongs_to :job
end
