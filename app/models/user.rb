class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  acts_as_messageable
  has_many :jobs
  has_many :posts
  has_many :bids
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

enum role: [:carrier, :shipper]         


def mailboxer_email(object)
  email
end
         
end
