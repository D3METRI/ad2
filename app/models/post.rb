class Post < ApplicationRecord
	belongs_to :user
	belongs_to :job
	accepts_nested_attributes_for :job
	accepts_nested_attributes_for :user
end
