class JobsController < ApplicationController
  before_action :set_job, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :edit]
  
  # GET /jobs
  # GET /jobs.json
  def index

if params[:search] and params[:search2]
        @starts = Job.near(params[:search], params[:distance] || 50, :order => "distance", latitude: :startlat, longitude: :startlng).sort_by(&:pickuptime).reverse
        @ends = Job.near(params[:search2], params[:distance2] || 50, :order => "distance", latitude: :endlat, longitude: :endlng).sort_by(&:pickuptime).reverse
        @jobs = @starts.sort_by(&:pickuptime).reverse & @ends.sort_by(&:pickuptime).reverse
        @jobs2 = @jobs.sort_by(&:pickuptime).reverse
     else          
       @starts = []
       @ends = []
       @jobs = []
      end

    @user = current_user

#    @hash = Gmaps4rails.build_markers(@jobs) do |job, marker|
#      job_link = view_context.link_to job.title, job_path(job)
#      marker.lat job.startlat
#      marker.lng job.startlng
#      marker.title job.title
#     marker.infowindow "<h4><u>#{job_link}</u></h4> 
#                       <i>Delivery from #{job.startad}<br></i>
#                       <i>to #{job.endad}"
#    end


  end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
    @job = Job.find(params[:id])
    @nearby_jobs = @job.nearbys(50) 
    @jobid = @job.id

    @newpost = @job.posts.new 
    @posts = Post.where(job_id: @jobid)

    @bids = @job.bids
  end

  # GET /jobs/new
  def new
    @job = Job.new
    @user = current_user
    @startadd = @job.build_startadd
    @endadd = @job.build_endadd

  end

  # GET /jobs/1/edit
  def edit
    @job = Job.find(params[:id])
    @user = current_user
    @trucktypes = ['Flatbed', 'Box']
    redirect_to jobs_path unless @job.user.id == @user.id
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new(job_params)

    respond_to do |format|
      if @job.save
        format.html { redirect_to @job, notice: 'Job was successfully created.' }
        format.json { render :show, status: :created, location: @job }
      else
        format.html { render :new }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /jobs/1
  # PATCH/PUT /jobs/1.json
  def update
    respond_to do |format|
      if @job.update(job_params)
        format.html { redirect_to @job, notice: 'Job was successfully updated.' }
        format.json { render :show, status: :ok, location: @job }
      else
        format.html { render :edit }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy

    redirect_to jobs_path unless @job.user.id == @user.id
    @user = current_user
    @job.destroy
    respond_to do |format|
      format.html { redirect_to jobs_url, notice: 'Job was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def bid
    @job = Job.find(params[:id])
    @user = current_user
    @bid = Bid.new
  end

  def post
    @job = Job.find(params[:id])
    @post = Post.new
    @user = current_user
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_params
      params.require(:job).permit(:status, :title, :description, :pickuptime, :dropofftime, :startad, :startlat, :startlng, :endad, :endlat, :endlng, :user_id, :length, :price, :trucktype, :weight, :size, posts_attributes: [:id, :body, :title, :_destroy], bid_attributes: [:id, :bidstatus, :bid, :details], startadd_attributes: [:id, :address, :lat, :lng], endadd_attributes: [:id, :address, :lat, :lng])
    end





end
