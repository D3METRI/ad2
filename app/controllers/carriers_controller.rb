class CarriersController < ApplicationController
  def index
  	@carrier = User.where(role: "carrier")
  end

  def show
  	@carrier = User.find(params[:id])
  	@user = current_user
  end
end
