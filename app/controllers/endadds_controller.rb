class EndaddsController < ApplicationController
  before_action :set_endadd, only: [:show, :edit, :update, :destroy]

  # GET /endadds
  # GET /endadds.json
  def index
    @endadds = Endadd.all
  end

  # GET /endadds/1
  # GET /endadds/1.json
  def show
  end

  # GET /endadds/new
  def new
    @endadd = Endadd.new
  end

  # GET /endadds/1/edit
  def edit
  end

  # POST /endadds
  # POST /endadds.json
  def create
    @endadd = Endadd.new(endadd_params)

    respond_to do |format|
      if @endadd.save
        format.html { redirect_to @endadd, notice: 'Endadd was successfully created.' }
        format.json { render :show, status: :created, location: @endadd }
      else
        format.html { render :new }
        format.json { render json: @endadd.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /endadds/1
  # PATCH/PUT /endadds/1.json
  def update
    respond_to do |format|
      if @endadd.update(endadd_params)
        format.html { redirect_to @endadd, notice: 'Endadd was successfully updated.' }
        format.json { render :show, status: :ok, location: @endadd }
      else
        format.html { render :edit }
        format.json { render json: @endadd.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /endadds/1
  # DELETE /endadds/1.json
  def destroy
    @endadd.destroy
    respond_to do |format|
      format.html { redirect_to endadds_url, notice: 'Endadd was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_endadd
      @endadd = Endadd.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def endadd_params
      params.require(:endadd).permit(:address, :lat, :lng, :job_id)
    end
end
