class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
	
rescue_from ActiveRecord::RecordNotFound do
  flash[:warning] = 'Resource not found.'
  redirect_back_or root_path
end

def redirect_back_or(path)
  redirect_to request.referer || path
end

  before_action :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters
  	#signup
  	devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:name, :phone, :email, :password, :company, :role) }
  	#editing
    update_attrs = [:name, :phone, :password, :password_confirmation, :current_password, :company, :role]
    devise_parameter_sanitizer.permit :account_update, keys: update_attrs
  end

	protected


end
