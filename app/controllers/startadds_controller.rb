class StartaddsController < ApplicationController
  before_action :set_startadd, only: [:show, :edit, :update, :destroy]

  # GET /startadds
  # GET /startadds.json
  def index
    @startadds = Startadd.all
  end

  # GET /startadds/1
  # GET /startadds/1.json
  def show
  end

  # GET /startadds/new
  def new
    @startadd = Startadd.new
  end

  # GET /startadds/1/edit
  def edit
  end

  # POST /startadds
  # POST /startadds.json
  def create
    @startadd = Startadd.new(startadd_params)

    respond_to do |format|
      if @startadd.save
        format.html { redirect_to @startadd, notice: 'Startadd was successfully created.' }
        format.json { render :show, status: :created, location: @startadd }
      else
        format.html { render :new }
        format.json { render json: @startadd.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /startadds/1
  # PATCH/PUT /startadds/1.json
  def update
    respond_to do |format|
      if @startadd.update(startadd_params)
        format.html { redirect_to @startadd, notice: 'Startadd was successfully updated.' }
        format.json { render :show, status: :ok, location: @startadd }
      else
        format.html { render :edit }
        format.json { render json: @startadd.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /startadds/1
  # DELETE /startadds/1.json
  def destroy
    @startadd.destroy
    respond_to do |format|
      format.html { redirect_to startadds_url, notice: 'Startadd was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_startadd
      @startadd = Startadd.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def startadd_params
      params.require(:startadd).permit(:address, :lat, :lng, :job_id)
    end
end
