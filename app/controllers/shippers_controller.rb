class ShippersController < ApplicationController

  def index
  	@shipper = User.where(role: "shipper")
  end

  def show
  	@shipper = User.find(params[:id])
  	@user = current_user
  end
end
