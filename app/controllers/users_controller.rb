class UsersController < ApplicationController
  before_action :authenticate_user!

  def index
	@users = User.all  	
  end

	def show
		@user = User.find(params[:id])
	end

	def carrier
		@carrier = User.where(role: "carrier")
	end

end
