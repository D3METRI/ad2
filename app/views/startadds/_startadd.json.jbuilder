json.extract! startadd, :id, :address, :lat, :lng, :job_id, :created_at, :updated_at
json.url startadd_url(startadd, format: :json)