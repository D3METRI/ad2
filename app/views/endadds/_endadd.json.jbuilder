json.extract! endadd, :id, :address, :lat, :lng, :job_id, :created_at, :updated_at
json.url endadd_url(endadd, format: :json)