json.extract! bid, :id, :details, :bid, :bidstatus, :created_at, :updated_at
json.url bid_url(bid, format: :json)