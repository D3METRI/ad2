json.extract! job, :id, :status, :title, :description, :pickuptime, :dropofftime, :startaddress, :endaddress, :lat, :lng, :created_at, :updated_at
json.url job_url(job, format: :json)