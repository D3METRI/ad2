class RemoveJobIdFromUsers < ActiveRecord::Migration[5.0]
  def change
    remove_reference :users, :job, foreign_key: true
  end
end
