class AddAttsToJobs < ActiveRecord::Migration[5.0]
  def change
    add_column :jobs, :size, :string
    add_column :jobs, :weight, :integer
    add_column :jobs, :trucktype, :string
    add_column :jobs, :price, :decimal, :precision => 8, :scale => 2
    add_column :jobs, :length, :string
  end
end
