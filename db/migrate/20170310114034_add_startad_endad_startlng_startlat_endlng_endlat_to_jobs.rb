class AddStartadEndadStartlngStartlatEndlngEndlatToJobs < ActiveRecord::Migration[5.0]
  def change
    add_column :jobs, :startad, :string
    add_column :jobs, :endad, :string
    add_column :jobs, :startlng, :decimal
    add_column :jobs, :startlat, :decimal
    add_column :jobs, :endlng, :decimal
    add_column :jobs, :endlat, :decimal
  end
end
