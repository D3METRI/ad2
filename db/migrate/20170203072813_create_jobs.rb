class CreateJobs < ActiveRecord::Migration[5.0]
  def change
    create_table :jobs do |t|
      t.integer :status, default: 0
      t.string :title
      t.text :description
      t.timestamp :pickuptime
      t.timestamp :dropofftime
      t.timestamps
    end
  end
end
