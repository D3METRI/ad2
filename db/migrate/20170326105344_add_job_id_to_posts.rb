class AddJobIdToPosts < ActiveRecord::Migration[5.0]
  def change
    add_reference :posts, :job, foreign_key: true
  end
end
