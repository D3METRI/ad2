class CreateStartadds < ActiveRecord::Migration[5.0]
  def change
    create_table :startadds do |t|
      t.string :address
      t.decimal :lat
      t.decimal :lng
      t.references :job, foreign_key: true

      t.timestamps
    end
  end
end
