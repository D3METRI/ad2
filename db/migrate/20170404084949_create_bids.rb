class CreateBids < ActiveRecord::Migration[5.0]
  def change
    create_table :bids do |t|
      t.string :details
      t.decimal :bid, :precision => 8, :scale => 2
      t.integer :bidstatus, default: 0
      t.references :user, index: true, foreign_key: true
      t.references :job, index: true, foreign_key: true

      t.timestamps
    end
  end
end
